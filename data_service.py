# 导入框架
import glob
import importlib
import os

from lib import plato
import json

# 初始化框架
frame = plato.get_plato().Init('lib/config/config.default.cfg', print, 'lib/rpc_config')


@frame.data.DataService
class DataServiceImpl:
    def __init__(self):
        self._cache = None
        self._persistent = None

    def _load_plugins(self):
        """
        加载插件, {name}_persistent.py为持久层插件, {name}_cache.py为缓存层插件, 缓存插件，持久插件分别最多只能有一个
        """
        for file in glob.glob(os.path.join('impl', "*.py")):
            file_name = os.path.splitext(os.path.basename(file))[0]
            if (file_name != "__init__") and (file_name.endswith('_cache') or file_name.endswith('_persistent')):
                spec = importlib.util.spec_from_file_location("." + file_name, file)
                mod = importlib.util.module_from_spec(spec)
                spec.loader.exec_module(mod)
                srv_impl = mod.Impl(frame)
                if srv_impl.start():
                    if file_name.endswith('cache'):
                        self._cache = srv_impl
                    elif file_name.endswith('persistent'):
                        self._persistent = srv_impl

    def OnAfterFork(self):
        self._load_plugins()

    def OnBeforeDestroy(self):
        if self._cache is not None:
            self._cache.stop()
        if self._persistent is not None:
            self._persistent.stop()

    def OnTick(self):
        if self._persistent is not None:
            self._persistent.tick()
        if self._cache is not None:
            self._cache.tick()

    def save(self, call, source):
        json_obj = json.loads(source)

        # 首先在redis中检测一下各个字段的更新时间
        filter_result = None
        if self._cache is not None:
            filter_result = yield from self._cache.update_time_filter(json_obj)
        
        if not json_obj['values']:
            return True

        if not filter_result:
            return False

        if self._persistent is not None:
            persist_save = yield from self._persistent.save(json_obj)
        else:
            persist_save = True
        cache_save = False
        if persist_save and self._cache is not None:
            cache_save = yield from self._cache.save(json_obj)
        return cache_save

    def load(self, call, source):
        json_obj = json.loads(source)
        cache_ret = None
        if json_obj['cmd'] == 'load_index':
            error_ret = "{}"
        else:
            error_ret = "[]"
        if self._cache is not None:
            cache_ret = yield from self._cache.load(json_obj)
        if cache_ret is not None:
            return cache_ret
        else:
            if self._persistent is not None:
                persist_ret = yield from self._persistent.load(json_obj)
                if persist_ret is None:
                    return error_ret
                if self._cache is not None:
                    if json_obj['cmd'] == 'load_index':
                        sync_obj = json.loads(persist_ret)
                        sync_obj['cmd'] = 'new'
                        sync_obj['type'] = json_obj['type']
                        sync_obj['index_name'] = json_obj['index_name']
                        sync_obj['index_type'] = json_obj['index_type']
                        sync_obj['index_value'] = json_obj['index_value']
                        sync_obj['expiration'] = json_obj['expiration']
                        yield from self._cache.sync(json_obj['type'], sync_obj)
                    elif json_obj['cmd'] == 'load_multi_index':
                        sync_obj = json.loads(persist_ret)
                        index_name = json_obj['index_name']
                        expiration = json_obj['expiration']
                        for values in sync_obj['multi_values']:
                            values['cmd'] = 'new'
                            values['type'] = json_obj['type']
                            values['index_name'] = index_name
                            values['index_type'] = json_obj['index_type']
                            values['expiration'] = expiration
                            for value in values['values']:
                                if value['name'] == index_name:
                                    index_value = value['value']
                                    if isinstance(index_value, str):
                                        values['index_value'] = index_value
                                    else:
                                        values['index_value'] = str(index_value)
                            yield from self._cache.sync(json_obj['type'], values)
                    else:
                        # 非指定数量的加载，不写入缓存，防止缓存耗尽
                        return persist_ret
                return persist_ret
            else:
                return error_ret

    def remove(self, call, source):
        json_obj = json.loads(source)
        remove_n = 0
        if self._persistent is not None:
            remove_n = yield from self._persistent.remove(json_obj)
        if self._cache is not None:
            if self._persistent is not None:
                # ignore cache count
                yield from self._cache.remove(json_obj)
            else:
                remove_n = yield from self._cache.remove(json_obj)
        return remove_n


# Register service
impl = DataServiceImpl()
frame.run_forever()
