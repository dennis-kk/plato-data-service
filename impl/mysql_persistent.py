import json
import traceback

import MySQLdb
import os
import sys

sys.path.append(os.path.dirname(__file__))

import async_task
import threading
import queue
import time
from plugin import Plugin


# todo 自动重连

class DbTask(async_task.AsyncTask):
    """
    MySQL数据库异步任务
    """

    def __init__(self, frame, cmd, co):
        """
        构造

        Args
            frame 框架引用
            cmd 操作命令JSON对象
            co 等待任务的协程
        """
        self.frame = frame
        self._cmd = cmd
        self._co = co
        self._cursor = None
        self._db = None
        self._result = None

    def set_cursor(self, cursor):
        """
        设置数据库操作游标引用
        """
        self._cursor = cursor

    def set_db(self, db):
        """
        设置数据库对象引用
        """
        self._db = db

    def resume(self):
        """
        恢复等待任务的协程继续执行
        """
        self._co.resume()

    def GetResult(self):
        """
        获取任务执行结果
        """
        return self._result


def get_table_type_string(value):
    """
    获取数据库类型定义
    """
    type_value = value['type']
    if type_value == 'int':
        return 'INT'
    elif type_value == 'uint':
        return 'INT UNSIGNED'
    elif type_value == 'int64':
        return 'BIGINT'
    elif type_value == 'uint64':
        return 'BIGINT UNSIGNED'
    elif type_value == 'float':
        return 'FLOAT'
    elif type_value == 'double':
        return 'DOUBLE'
    elif type_value == 'string':
        return 'VARCHAR(255)'


def get_table_type_default(value):
    """
    获取数据库默认值定义
    """
    type_value = value['type']
    if 'default' in value:
        default_value = value['default']
    else:
        return ""
    if type_value == 'int':
        return 'DEFAULT ' + default_value
    elif type_value == 'uint':
        return 'DEFAULT ' + default_value
    elif type_value == 'int64':
        return 'DEFAULT ' + default_value
    elif type_value == 'uint64':
        return 'DEFAULT ' + default_value
    elif type_value == 'float':
        return 'DEFAULT ' + default_value
    elif type_value == 'double':
        return 'DEFAULT ' + default_value
    elif type_value == 'string':
        return 'DEFAULT ' + "'" + default_value + "'"


def get_table_init(value, comma):
    """
    获取数据库字段定义
    """
    s = value['name'] + ' ' + get_table_type_string(value) + ' ' + get_table_type_default(value)
    if comma:
        s += ',\n'
    return s


def get_table_field_value_string_by_type(value):
    """
    获取数据库更新语句字段值
    """
    type_value = value['type']
    if type_value == 'string':
        return "'{}'".format(value['value'])
    else:
        return value['value']


def get_table_index_value(value):
    """
    获取数据库索引值
    """
    type_value = value['index_type']
    if type_value == 'string':
        return "'{}'".format(value['index_value'])
    else:
        return value['index_value']


GlobalSchema = {}


class DbLoadTask(DbTask):
    def __init__(self, frame, cmd, co):
        super().__init__(frame, cmd, co)

    def _get_field_names(self, name):
        global GlobalSchema
        if name in GlobalSchema:
            field_name_res = GlobalSchema[name]
        else:
            field_name_sql = "select `column_name` from `INFORMATION_SCHEMA`.`COLUMNS`  WHERE `TABLE_NAME`='{}'".format(
                name)
            try:
                self._cursor.execute(field_name_sql)
            except Exception as ex:
                errno = self._db.errno()
                if errno == 2006 or errno == 2013:
                    try:
                        self._db.ping(True)
                        self._cursor = self._db.cursor()
                        self._cursor.execute(field_name_sql)
                    except Exception as ex:
                        error = str(ex)
                        self.frame.write_warn_log('Invalid command:' + error)
                        self.frame.write_warn_log(traceback.format_exc())
                        return None
                else:
                    error = str(ex)
                    self.frame.write_warn_log('Invalid command:' + error)
                    self.frame.write_warn_log(traceback.format_exc())
                    return None
            field_name_res = self._cursor.fetchall()
            if field_name_res is not None:
                GlobalSchema[name] = field_name_res
        return field_name_res

    def _process_result(self, results, field_name_res):
        if self._cmd['cmd'] == 'load_index':
            ret_result = {}
            for row in results:
                field_list = []
                field_n = len(row)
                i = 0
                while i < field_n:
                    field_list.append({'name': field_name_res[i][0], 'value': row[i]})
                    i += 1
                ret_result['values'] = field_list
                self._result = json.dumps(ret_result)
                break
        elif self._cmd['cmd'] == 'load_multi_index':
            multi_ret = []
            for row in results:
                result = {'values': []}
                field_list = result['values']
                field_n = len(row)
                i = 0
                while i < field_n:
                    field_list.append({'name': field_name_res[i][0], 'value': row[i]})
                    i += 1
                multi_ret.append(result)
            self._result = json.dumps({'multi_values': multi_ret})
        elif self._cmd['cmd'] == 'load_all':
            ret_result = []
            for row in results:
                result = {'values': []}
                field_n = len(row)
                i = 0
                while i < field_n:
                    result['values'].append({'name': field_name_res[i][0], 'value': row[i]})
                    i += 1
                ret_result.append(result)
            self._result = json.dumps(ret_result)

    def DoTask(self):
        field_name_res = self._get_field_names(self._cmd['type'])
        if field_name_res is None:
            if self._cmd['cmd'] == 'load_index':
                self._result = '{}'
            else:
                self._result = '[]'
            return None
        sql = ""
        table_name = self._cmd['type']
        if self._cmd['cmd'] == 'load_index':
            sql = "SELECT * FROM {} WHERE {}={}".format(table_name, self._cmd['index_name'], get_table_index_value(self._cmd))
        elif self._cmd['cmd'] == 'load_multi_index':
            sql = "SELECT * FROM " + self._cmd['type'] + " WHERE " + self._cmd['index_name'] + " IN ("
            n = 1
            length = len(self._cmd['multi_index_value'])
            for v in self._cmd['multi_index_value']:
                if self._cmd['index_type'] == 'string':
                    sql += "'" + v + "'"
                else:
                    sql += v
                if n < length:
                    sql += ','
                n += 1
            sql += ")"
        else:
            sql = "SELECT * FROM " + self._cmd['type']
            if 'filter' in self._cmd:
                sql += ' WHERE ' + self._cmd['filter']
        try:
            self._cursor.execute(sql)
            self._db.commit()
        except Exception as ex:
            errno = self._db.errno()
            if errno == 2006 or errno == 2013:
                try:
                    self._db.ping(True)
                    self._cursor = self._db.cursor()
                    self._cursor.execute(sql)
                    self._db.commit()
                except Exception as ex:
                    self.frame.write_warn_log('Invalid command:' + str(ex))
                    self.frame.write_warn_log('SQL:' + sql)
                    self.frame.write_warn_log(traceback.format_exc())
                    if self._cmd['cmd'] == 'load_index':
                        self._result = '{}'
                    else:
                        self._result = '[]'
                    return
            else:
                self.frame.write_warn_log('Invalid command:' + str(ex))
                self.frame.write_warn_log('SQL:' + sql)
                self.frame.write_warn_log(traceback.format_exc())
                if self._cmd['cmd'] == 'load_index':
                    self._result = '{}'
                else:
                    self._result = '[]'
                return

        results = self._cursor.fetchall()
        self._cursor.close()
        self._process_result(results, field_name_res)


class DbSaveTask(DbTask):
    def __init__(self, frame, cmd, co):
        super().__init__(frame, cmd, co)

    def DoTask(self):
        sql = ''
        if self._cmd['cmd'] == 'new':
            sql = 'INSERT INTO ' + self._cmd['type'] + ' VALUES('
            n = len(self._cmd['values'])
            i = 1
            for value in self._cmd['values']:
                sql += get_table_field_value_string_by_type(value)
                if i < n:
                    sql += ','
                i += 1
            sql += ')'
        else:
            sql = 'UPDATE ' + self._cmd['type']
            n = 1
            field_n = len(self._cmd['values'])
            for value in self._cmd['values']:
                if n == 1:
                    sql += ' SET '
                sql += '`' + value['name'] + '`=' + get_table_field_value_string_by_type(value)
                if n < field_n:
                    sql += ','
                n += 1
            sql += " WHERE "
            if 'filter' in self._cmd:
                sql += self._cmd['filter']
            else:
                sql += self._cmd['index_name'] + "=" + get_table_index_value(self._cmd)
        try:
            self._cursor.execute(sql)
            self._db.commit()
            self._cursor.close()
            self._result = True
        except Exception as ex:
            errno = self._db.errno()
            if errno == 2006 or errno == 2013:
                try:
                    self._db.ping(True)
                    self._cursor = self._db.cursor()
                    self._cursor.execute(sql)
                    self._db.commit()
                    self._cursor.close()
                    self._result = True
                except Exception as ex:
                    self._db.rollback()
                    self.frame.write_warn_log('Invalid command:' + str(ex))
                    self.frame.write_warn_log(traceback.format_exc())
                    self._result = False
            else:
                self._db.rollback()
                self.frame.write_warn_log('Invalid command:' + str(ex))
                self.frame.write_warn_log(traceback.format_exc())
                self._result = False


class DbRemoveTask(DbTask):
    def __init__(self, frame, cmd, co):
        super().__init__(frame, cmd, co)

    def DoTask(self):
        sql = 'DELETE FROM ' + self._cmd['type']
        sql += " WHERE "
        if 'filter' in self._cmd:
            sql += self._cmd['filter']
        else:
            sql += self._cmd['index_name'] + "=" + get_table_index_value(self._cmd)
        try:
            self._cursor.execute(sql)
            self._db.commit()
            self._cursor.close()
            self._result = 1
        except Exception as ex:
            errno = self._db.errno()
            if errno == 2006 or errno == 2013:
                try:
                    self._db.ping(True)
                    self._cursor = self._db.cursor()
                    self._cursor.execute(sql)
                    self._db.commit()
                    self._cursor.close()
                    self._result = 1
                except Exception as ex:
                    self._db.rollback()
                    self.frame.write_warn_log('Invalid command:' + str(ex))
                    self.frame.write_warn_log(traceback.format_exc())
                    self._result = 0
            else:
                self._db.rollback()
                self.frame.write_warn_log('Invalid command:' + str(ex))
                self.frame.write_warn_log(traceback.format_exc())
                self._result = 0


class DbQueueThread(threading.Thread):
    """
    线程池, 用于执行异步任务
    """

    def __init__(self, frame):
        threading.Thread.__init__(self)
        self.frame = frame
        self._input_queue = queue.Queue()
        self._output_queue = queue.Queue()
        self._stop_flag = False
        self._get_config()
        self._db = MySQLdb.connect(host=self._host, port=self._port, user=self._user, passwd=self._passwd,
                                   db=self._db_name, charset=self._charset)

    def _get_config(self):
        self._host = self.frame.get_config("persistent.mysql.host")
        if self._host is None:
            self._host = "localhost"
        self._port = self.frame.get_config("persistent.mysql.port")
        if self._port is None:
            self._port = 3306
        self._user = self.frame.get_config("persistent.mysql.user")
        self._passwd = self.frame.get_config("persistent.mysql.passwd")
        self._db_name = self.frame.get_config("persistent.mysql.db")
        self._charset = self.frame.get_config("persistent.mysql.charset")
        if self._charset is None:
            self._charset = "utf8"

    def push(self, task):
        self._input_queue.put(task)

    def get_no_wait(self):
        try:
            return self._output_queue.get_nowait()
        except queue.Empty:
            return None

    def stop(self):
        self._db.close()
        self._stop_flag = True

    def run(self):
        while not self._stop_flag:
            try:
                task: DbTask = self._input_queue.get_nowait()
                task.set_cursor(self._db.cursor())
                task.set_db(self._db)
                task.DoTask()
                self._output_queue.put(task)
            except queue.Empty:
                time.sleep(0.01)
            except Exception as e:
                self.frame.write_error_log(str(e))
                self.frame.write_warn_log(traceback.format_exc())


def create_worker(frame):
    return DbQueueThread(frame)


class Impl(Plugin):
    def __init__(self, frame):
        super().__init__(frame)
        self._pool = None

    def start(self):
        mysql_cfg = self.frame.get_config("persistent.mysql")
        if mysql_cfg is None:
            return False
        self.frame.write_verb_log('MySQL started')
        self._pool = async_task.AsyncTaskThreadingPool(self.frame, WorkerFactory=create_worker)
        return True

    def stop(self):
        self._pool.stop()
        self.frame.write_verb_log('MySQL stopped')

    def tick(self):
        while True:
            # 从队列内去完成的任务
            task = self._pool.get_no_wait()
            if task is None:
                return
            else:
                # 唤醒协程
                task.resume()

    def save(self, json_obj):
        try:
            task = DbSaveTask(self.frame, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return False

    def load(self, json_obj):
        try:
            task = DbLoadTask(self.frame, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return 0

    def remove(self, json_obj):
        try:
            task = DbRemoveTask(self.frame, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return 0
