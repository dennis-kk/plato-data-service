import traceback

import redis
import json
from async_task import *
from plugin import CachePlugin


class RedisTask(AsyncTask):
    def __init__(self, frame, pipeline, cmd, co):
        self.frame = frame
        self._pipeline = pipeline
        self._cmd = cmd
        self._co = co

    def resume(self):
        self._co.resume()

    def GetResult(self):
        return None


class RedisSaveTask(RedisTask):
    def __init__(self, frame, pipeline, cmd, co):
        super().__init__(frame, pipeline, cmd, co)
        self._result = True

    def DoTask(self):
        try:
            pipeline = self._pipeline.pipeline(transaction=False)
            table_name = self._cmd['type']
            index = self._cmd['index_value']
            hash_key = table_name + ':' + index
            expiration = self._cmd['expiration']
            for kv in self._cmd['values']:
                pipeline.hset(hash_key, kv['name'], kv['value'])
            if expiration > 0:
                pipeline.expire(hash_key, expiration)
            pipeline.execute()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            self._result = False

    def GetResult(self):
        return self._result


class RedisLoadTask(RedisTask):
    def __init__(self, frame, pipeline, cmd, co):
        super().__init__(frame, pipeline, cmd, co)
        self._result = 0

    def DoTask(self):
        try:
            pipeline = self._pipeline.pipeline(transaction=False)
            expiration = self._cmd['expiration']
            if self._cmd['cmd'] == 'load_index':
                table_name = self._cmd['type']
                index = self._cmd['index_value']
                hash_key = table_name + ':' + index
                pipeline.hgetall(hash_key)
                if expiration > 0:
                    pipeline.expire(hash_key, expiration)
                list_ret = pipeline.execute()
                kv_list = []
                for hash_ret in list_ret:
                    if not isinstance(hash_ret,dict):
                        continue
                    for k, v in hash_ret.items():
                        kv_list.append({"name": k, "value": v})
                if len(kv_list) > 0:
                    self._result = json.dumps({'index': index, 'values': kv_list})
                else:
                    self._result = None
            elif self._cmd['cmd'] == 'load_multi_index':
                table_name = self._cmd['type']
                for index_value in self._cmd['multi_index_value']:
                    hash_key = '{}:{}'.format(table_name, index_value)
                    pipeline.hgetall(hash_key)
                    if expiration > 0:
                        pipeline.expire(hash_key, expiration)
                list_ret = pipeline.execute()
                multi_value = []
                for hash_ret in list_ret:
                    if not isinstance(hash_ret,dict):
                        continue
                    kv_list = []
                    for k, v in hash_ret.items():
                        kv_list.append({"name": k, "value": v})
                    if len(kv_list) > 0:
                        multi_value.append({'values': kv_list})
                if len(multi_value) > 0:
                    self._result = json.dumps({'multi_values': multi_value})
                else:
                    self._result = None
            else:
                self._result = None
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            self._result = None

    def GetResult(self):
        return self._result


class RedisRemoveTask(RedisTask):
    def __init__(self, frame, pipeline, cmd, co):
        super().__init__(frame, pipeline, cmd, co)
        self._result = 0

    def DoTask(self):
        try:
            pipeline = self._pipeline.pipeline(transaction=False)
            table_name = self._cmd['type']
            index = self._cmd['index_value']
            hash_key = table_name + ':' + index
            pipeline.delete(hash_key)
            pipeline.execute()
            self._result = 1
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)

    def GetResult(self):
        return self._result

class RedisFilterTask(RedisTask):
    def __init__(self, frame, pipeline, cmd, co, sha):
        super().__init__(frame, pipeline, cmd, co)
        self._script_sha = sha
        self._result = True

    def DoTask(self):
        try:
            pipeline = self._pipeline.pipeline(transaction=False)
            table_name = self._cmd['type']
            index = self._cmd['index_value']
            hash_key = table_name + '_UT:' + index

            for kv in self._cmd['values']:
                if 'updatetime' not in kv.keys():
                    return
                key_str = kv['name']+"_updatetime"
                result_eval = pipeline.evalsha(self._script_sha, 2, hash_key, key_str, kv['updatetime'])
                kv.pop('updatetime')
            result = pipeline.execute()

            remove_dict = {}
            for index in range(0, len(result)):
                if result[index] == -1:
                    kv = self._cmd['values'][index]
                    remove_dict[kv['name']] = True

            # 筛选掉旧数据
            if remove_dict:
                self._cmd['values'] = list(filter(lambda kv: kv['name'] not in remove_dict.keys() , self._cmd['values']))
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            self._result = False

    def GetResult(self):
        return self._result

    def GetCmd(self):
        return self._cmd

class Impl(CachePlugin):
    def __init__(self, frame):
        super().__init__(frame)
        self._redis = None
        self._pool = None
        self._check_expire_time = get_ms_unix_time_stamp() + 60000
        self._script_sha = None

    def start(self):
        redis_cfg = self.frame.get_config("cache.redis")
        if redis_cfg is None:
            return False
        host = self.frame.get_config("cache.redis.host")
        port = self.frame.get_config("cache.redis.port")
        pool = redis.ConnectionPool(host=host, port=port, decode_responses=True)
        self._redis = redis.Redis(connection_pool=pool)
        self.frame.write_verb_log('Redis started')
        self._pool = AsyncTaskThreadingPool(self.frame)
        return True

    def stop(self):
        self._pool.stop()
        self.frame.write_verb_log('Redis stopped')

    def tick(self):
        while True:
            if self._check_expire_time < get_ms_unix_time_stamp():
                LogInfo("redis_cache_pool:%s" % self._pool.queues_size_info())
                self._check_expire_time = get_ms_unix_time_stamp() + 60000

            # 从队列内去完成的任务
            task = self._pool.get_no_wait()
            if task is None:
                return
            else:
                # 唤醒协程
                task.resume()

    def save(self, json_obj):
        try:
            pipeline = self._redis.pipeline(transaction=False)
            task = RedisSaveTask(self.frame, pipeline, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return False

    def load(self, json_obj):
        if json_obj['cmd'] == 'load_all':
            # load_all is unsupported
            return None
        try:
            pipeline = self._redis.pipeline(transaction=False)
            task = RedisLoadTask(self.frame, pipeline, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return None

    def sync(self, type_name, json_obj):
        if isinstance(json_obj, list):
            # load_all
            for row in json_obj:
                yield from self.save(row)
        elif isinstance(json_obj, dict):
            yield from self.save(json_obj)
        return True

    def remove(self, json_obj):
        try:
            pipeline = self._redis.pipeline(transaction=False)
            task = RedisRemoveTask(self.frame, pipeline, json_obj, self.frame.coro())
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return 0

    def update_time_filter(self, json_obj):
        try:
            if self._script_sha is None:
                lua_script = "local v = redis.call('hget', KEYS[1], KEYS[2])  if v == false or v < ARGV[1] then return redis.call('hset', KEYS[1], KEYS[2], ARGV[1]) else return -1 end"
                self._script_sha = self._redis.script_load(lua_script)
            pipeline = self._redis.pipeline(transaction=False)
            task = RedisFilterTask(self.frame, pipeline, json_obj, self.frame.coro(), self._script_sha)
            self._pool.push(task)
            # 出让当前协程
            yield self.frame.wait()
            # 协程恢复返回结果
            json_obj = task.GetCmd()
            return task.GetResult()
        except Exception as ex:
            error = str(ex)
            self.frame.write_warn_log('Invalid command:' + error)
            self.frame.write_warn_log(traceback.format_exc())
            return False